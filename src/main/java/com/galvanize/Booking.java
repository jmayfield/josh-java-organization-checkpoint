package com.galvanize;

public class Booking {
    private String type;
    private int roomNumber;
    private String startTime;
    private String endTime;

    public Booking(String type, int roomNumber, String startTime, String endTime) {
        this.type = type;
        this.roomNumber = roomNumber;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public static Booking parse(String arg) {
    }

    public String getType() {
        return type;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }
}