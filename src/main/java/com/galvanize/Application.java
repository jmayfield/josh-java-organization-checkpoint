package com.galvanize;


import com.galvanize.Formatter.CSVFormatter;
import com.galvanize.Formatter.HTMLFormatter;
import com.galvanize.Formatter.JSONFormatter;

public class Application {
    public static void main(String[] args) {
        Formatters formatters = getFormatter(args[1]);
        Booking booking = Booking.parse(args[0]);


        System.out.println(formatters.format(booking));
    }

    public static Formatters getFormatter(String format) {
        switch (format) {
            case "json":
                return (Formatters) new JSONFormatter();
            case "csv":
                return (Formatters) new CSVFormatter();
            case "html":
                return (Formatters) new HTMLFormatter();
            default:
                return null;
        }
    }
}
