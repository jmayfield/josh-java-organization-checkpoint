package com.galvanize;

interface Formatters {
    String format(Booking booking);
}