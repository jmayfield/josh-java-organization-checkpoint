package com.galvanize.Formatter;

import com.galvanize.Booking;

import java.util.Formatter;

public class CSVFormatter implements Formatter {
    public String format(Booking booking) {
        return "type,room number,start time,end time\n" + booking.getType() + "," + booking.getRoomNumber() + "," + booking.getStartTime() + "," + booking.getEndTime();
    }
}
