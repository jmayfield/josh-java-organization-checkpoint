package com.galvanize.Formatter;

import com.galvanize.Booking;

import java.util.Formatter;

public class JSONFormatter implements Formatter {
    public String format(Booking booking) {
        return "{\n  \"type\": \"" + booking.getType() + "\",\n  \"roomNumber\": " + booking.getRoomNumber() + ",\n  \"startTime\": \"" + booking.getStartTime() + "\",\n  \"endTime\": \"" + booking.getEndTime() + "\"\n}";
    }
}
